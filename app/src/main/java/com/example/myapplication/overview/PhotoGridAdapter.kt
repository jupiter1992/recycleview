package com.example.myapplication.overview


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.databinding.GridViewItemBinding
import com.example.myapplication.databinding.GridViewItemHeaderBinding
import com.example.myapplication.network.MarsProperty

class PhotoGridAdapter(private val click: OnClickListener): ListAdapter<MarsProperty, RecyclerView.ViewHolder>(DiffCallback) {
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MarsPropertyViewHolder) {
            (holder).bind(getItem(position))
        }
    }

    enum class ViewType (val value: Int) {
        Normal(0),
        Header(1)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == ViewType.Normal.value) {
            MarsPropertyViewHolder(GridViewItemBinding.inflate(LayoutInflater.from(parent.context)))

        } else {
            MarsHeaderViewHolder(GridViewItemHeaderBinding.inflate(LayoutInflater.from(parent.context)))

        }
    }


    override fun getItemViewType(position: Int): Int {
        val marsProperty = getItem(position)
        return if (marsProperty.isRental) {
            ViewType.Normal.value
        } else {
            ViewType.Header.value
        }
    }

    class MarsPropertyViewHolder(private val binding: GridViewItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(marsProperty: MarsProperty) {
            binding.property = marsProperty
            binding.executePendingBindings()


        }

    }
    class MarsHeaderViewHolder(private val binding: GridViewItemHeaderBinding): RecyclerView.ViewHolder(binding.root)



    companion object DiffCallback: DiffUtil.ItemCallback<MarsProperty>() {
        override fun areItemsTheSame(oldItem: MarsProperty, newItem: MarsProperty): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: MarsProperty, newItem: MarsProperty): Boolean {
            return oldItem.id == newItem.id
        }

    }

}
class OnClickListener(val clickListener: (marsProperty: MarsProperty) -> Unit) {
    fun onClick(marsProperty: MarsProperty) = clickListener(marsProperty)
}

class ClickListener<T>(val clickListener: (value: T) -> Unit) {
    fun onClick(value: T) = clickListener(value)
}

abstract class RecycleViewAdapter<T,R: RecyclerView.ViewHolder,C: DiffCallBack<T> >(callBack: DiffCallBack<T>): ListAdapter<T,R>(callBack) {

}
abstract class DiffCallBack<T>: DiffUtil.ItemCallback<T>() {

}
