package com.example.myapplication.overview

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication.network.MarsApi
import com.example.myapplication.network.MarsProperty
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.nio.channels.Selector

enum class MarsApiStatus {
    LOADING, ERROR, DONE
}
enum class MarsApiFilter(val value: String) {
    SHOW_RENT("rent"),
    SHOW_BUY("buy"),
    SHOW_ALL("all")
}
class OverViewVM: ViewModel() {
    private lateinit var itemSelector: Selector

    private val _response = MutableLiveData<String>()
    private val _properties = MutableLiveData<List<MarsProperty>>()
    private val _status = MutableLiveData<MarsApiStatus>()
    private val _selectedMarsProperty = MutableLiveData<MarsProperty>()
    val selectedMarsProperty: LiveData<MarsProperty>
    get() = _selectedMarsProperty

    val status: LiveData<MarsApiStatus>

        get() = _status



    val properties: LiveData<List<MarsProperty>>
        get() = _properties

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)
    val response: LiveData<String>

    get() = _response
    init {
        _status.value = MarsApiStatus.LOADING
        getMarsRealEstateProperties(MarsApiFilter.SHOW_ALL)
    }
    private fun getMarsRealEstateProperties(filter: MarsApiFilter) {
        _response.value = "Set the Mars API Response here!"
        coroutineScope.launch {
            var getPropertiesDeferred = MarsApi.marsApiService.getProperties(filter.value)
            try {
                _status.value = MarsApiStatus.LOADING
                var listResult = getPropertiesDeferred.await()
                _status.value = MarsApiStatus.DONE
                _response.value = listResult.size.toString()
                _properties.value = listResult

            }
            catch (e: Exception) {
                _status.value = MarsApiStatus.ERROR
                _properties.value = listOf()
            }
        }


    }

    fun updateFilter(filter: MarsApiFilter) {
        getMarsRealEstateProperties(filter)
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun displayPropertyDetails(marsProperty: MarsProperty) {
        _selectedMarsProperty.value = marsProperty
    }

    fun displayPropertyDetailsComplete() {
        _selectedMarsProperty.value = null
    }


}