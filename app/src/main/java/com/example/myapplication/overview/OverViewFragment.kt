package com.example.myapplication.overview


import android.os.Bundle
import android.view.*
import android.widget.HeaderViewListAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController

import com.example.myapplication.R
import com.example.myapplication.databinding.FragmentOverViewBinding

/**
 * A simple [Fragment] subclass.
 */
class OverViewFragment : Fragment() {
    private lateinit var viewModel: OverViewVM

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        setHasOptionsMenu(true)

        val binding = FragmentOverViewBinding.inflate(inflater)
        viewModel = ViewModelProviders.of(this).get(OverViewVM ::class.java)

        binding.viewModel = viewModel
        binding.photoGrid.adapter = PhotoGridAdapter(OnClickListener { marsProperty ->
            viewModel.displayPropertyDetails(marsProperty)
        } )
        viewModel.selectedMarsProperty.observe(this, Observer {
            if (null != it) {
                findNavController().navigate(OverViewFragmentDirections.actionOverViewFragmentToDetailFragment(viewModel.selectedMarsProperty.value ?: return@Observer,"aaa"))

                viewModel.displayPropertyDetailsComplete()
            }
        })
        binding.lifecycleOwner = this


        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.overflow_menu,menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.show_all_menu -> {
                viewModel.updateFilter(MarsApiFilter.SHOW_ALL)
            }
            R.id.show_buy_menu -> {
                viewModel.updateFilter(MarsApiFilter.SHOW_BUY)
            }
            R.id.show_rent_menu -> {
                viewModel.updateFilter(MarsApiFilter.SHOW_RENT)
            }
        }

        return true
    }



}
