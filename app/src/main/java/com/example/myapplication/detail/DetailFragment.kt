package com.example.myapplication.detail


import android.app.Application
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders

import com.example.myapplication.R
import com.example.myapplication.databinding.FragmentDetailBinding

/**
 * A simple [Fragment] subclass.
 */
class DetailFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentDetailBinding = FragmentDetailBinding.inflate(inflater)
        val marsProperty = DetailFragmentArgs.fromBundle(arguments ?: return binding.root).selectedProperty

        val application = (activity)?.application ?: Application()
        val viewModelFactory = DetailViewModelFactory(marsProperty,application)
        binding.viewModel = ViewModelProviders.of(this,viewModelFactory).get(DetailViewModel ::class.java)
        binding.lifecycleOwner = this
        return binding.root
    }


}
